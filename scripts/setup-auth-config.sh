#!/usr/bin/env sh
app_name=demo
secret_root_path=applications
app_secret=demo

echo "[setup-auth-config] -- Using kv-v2 Vault secrets"
vault secrets enable -path="$secret_root_path" kv-v2
echo "[setup-auth-config] -- Generating default secret"
vault kv put "$secret_root_path/$app_secret" username="db-readonly-username" password="db-secret-password"
echo "[setup-auth-config] -- Enabling kubernetes auth for Vault"
vault auth enable kubernetes
echo "[setup-auth-config] -- Setting up auth/kubernetes/config"
vault write auth/kubernetes/config \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
    token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt \
    issuer="https://kubernetes.default.svc.cluster.local"

echo "[setup-auth-config] -- Creating vault policy for $app_name"
vault policy write $app_name - <<EOF
path "$secret_root_path/data/$app_secret" {
  capabilities = ["read"]
}
EOF

echo "[setup-auth-config] -- Writing $app_name role into Vault for namespace vault-demos"
vault write auth/kubernetes/role/$app_name \
    bound_service_account_names=$app_name \
    bound_service_account_namespaces=vault-demos \
    policies=$app_name \
    ttl=24h

