#!/usr/bin/env sh

port=${REGISTRY_PORT:=41906}
if k3d registry list | grep -q "k3d-registry.localhost"; then
  echo "Registry already exists, skipping creation..."
else
  echo "Creating Registry for container images..."
  k3d registry create registry.localhost --port "$port"
fi

if k3d cluster list | grep -q "ucsdlibrary-dev"; then
  echo "ucsdlibrary-dev cluster already exists, skipping creation..."
else
  echo "Creating cluster for ucsdlibrary-dev..."
  k3d cluster create ucsdlibrary-dev \
    --api-port 6550 \
    --registry-use k3d-registry.localhost:"$port" \
    --servers 1 \
    --agents 3 \
    --port 80:80@loadbalancer \
    --volume "$(pwd)":/src@all \
    --wait
fi


echo "Installing HashiCorp vault into vault-demos namespace..."
namespace="vault-demos"
context="k3d-ucsdlibrary-dev"

if kubectl --context $context get namespaces | grep -q "patron-load-development"; then
  echo "Namespace $namespace already exists, skipping creation..."
else
  echo "Creating namespace for deployment..."
  kubectl --context $context create namespace "$namespace"
fi

helm repo add hashicorp https://helm.releases.hashicorp.com
helm repo update

