#!/usr/bin/env sh
echo "Uninstalling release: csi-vault..."
context="k3d-ucsdlibrary-dev"
namespace="vault-demos"
helm delete \
  --kube-context="$context" \
  --namespace="$namespace" \
  csi-vault
