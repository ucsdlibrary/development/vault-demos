#!/usr/bin/env sh
context="k3d-ucsdlibrary-dev"
namespace="vault-demos"
pod="sidecar-vault-0"

prefix="kubectl --context ${context} --namespace ${namespace}"
$prefix rollout status deployment/sidecar-vault-agent-injector

echo "Setting up Vault secret and Kubernetes authentication..."
$prefix cp scripts/setup-auth-config.sh "$pod":/vault
$prefix exec -it "$pod" -- /bin/sh /vault/setup-auth-config.sh

echo "Creating ServiceAccount for Vault/App named: demo..."
$prefix create sa demo

echo "Deploying secret env vars demo..."
$prefix apply -f sidecar-deployment.yaml
# $prefix delete -f sidecar-deployment.yaml
