#!/usr/bin/env sh
echo "Installing Vault for CSI provider usage. release: csi-vault..."
context="k3d-ucsdlibrary-dev"
namespace="vault-demos"
helm upgrade \
  --install \
  --kube-context="$context" \
  --namespace="$namespace" \
  --values scripts/csi-values.yaml \
  csi-vault hashicorp/vault
