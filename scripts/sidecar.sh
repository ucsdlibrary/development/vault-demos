#!/usr/bin/env sh
echo "Installing Vault for sidecar usage. release: sidecar-vault..."
context="k3d-ucsdlibrary-dev"
namespace="vault-demos"

helm upgrade \
  --install \
  --kube-context="$context" \
  --namespace="$namespace" \
  --values scripts/sidecar-values.yaml \
  sidecar-vault hashicorp/vault
