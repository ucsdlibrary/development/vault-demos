#!/usr/bin/env sh
echo "Uninstalling release: sidecar-vault..."
context="k3d-ucsdlibrary-dev"
namespace="vault-demos"
helm delete \
  --kube-context="$context" \
  --namespace="$namespace" \
  sidecar-vault

prefix="kubectl --context ${context} --namespace ${namespace}"
$prefix delete -f sidecar-deployment.yaml
