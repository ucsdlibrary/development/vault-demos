#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
image_tag="k3d-registry.localhost:$registry_port/vault_demo:latest"

echo "Building vault_demo image..."
docker build -t "$image_tag" -f Dockerfile .

echo "Pushing vault_demo image to local registry..."
docker push "$image_tag"
