#!/usr/bin/env sh

# source env vars from vault secret file
if [ "$VAULT_FILE" ] && [ -e "$VAULT_FILE" ]; then
	. "$VAULT_FILE"
fi

exec "$@"
