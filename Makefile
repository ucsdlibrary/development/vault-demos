.PHONY: run menu

menu:
	@echo 'build: Build vault_demo image to check env vars from vault'
	@echo 'csi: Install HashiCorp vault for CSI Provider usage'
	@echo 'drop-csi: Uninstall HashiCorp vault for CSI Provider usage'
	@echo 'drop-sidecar: Uninstall HashiCorp vault for Agent sidecar usage'
	@echo 'setup-sidecar: Setup the Vault for sidecar demo and Deployment'
	@echo 'setup: Create a k3d/k3s cluster'
	@echo 'sidecar: Install HashiCorp vault for Agent sidecar usage'

build:
	scripts/build.sh
drop-sidecar:
	scripts/drop-sidecar.sh
drop-csi:
	scripts/drop-csi.sh
setup:
	scripts/setup.sh
sidecar:
	scripts/sidecar.sh
setup-sidecar:
	scripts/setup-sidecar.sh
csi:
	scripts/csi.sh
