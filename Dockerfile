FROM ruby:3.0.3-alpine

RUN apk add build-base && \
  gem install puma && \
  gem install sinatra

WORKDIR /demo
COPY scripts/app.rb /demo/
COPY scripts/command.sh /demo/
COPY scripts/entrypoint.sh /demo/

ENTRYPOINT ["/demo/entrypoint.sh"]
CMD ["ruby", "app.rb", "-s", "Puma"]

