This is meant to help me (and perhaps others) understand the options for using
[Hashicorp Vault][vault] within the context of Kubernetes applications and deployments.

We are going to be deploying a local Kubernetes cluster using the following
tools:

* [Helm][helm]
* [k3d][k3d]

To setup the Kubernetes cluster:

```sh
make setup
```

To follow the [Injecting Secrets into Kubernetes Pods tutorial][agent-tutorial] run:

```sh
make sidecar
```

If you want to automatically run the commands from the `sidecar` tutorials and
deploy a simple Puma application to test with, you can run:

```sh
make build
make setup-sidecar
```

To follow the [CSI tutorial][csi-tutorial] run:

```sh
make csi
```

To uninstall a tutorial you can use either:

```sh
make drop-sidecar
make drop-csi
```

There seems to be, at the time of this writing, 2 primary ways of exposing Vault
secrets to Kubernetes pods/applications.

1. [Injecting Secrets into Kubernetes Pods via Vault Agent Containers][agent-tutorial]
1. [Retrieve HashiCorp Vault Secrets with Kubernetes CSI][csi-tutorial]

We will be following the tutorials exactly as written, the primary change will
be using `k3d` instead of `minikube`, so if `minikube` is a better fit for your
Operating System then you can/should follow the tutorial exactly as written.

Takeaways:
- Any of our existing Helm charts will require some modification
- Sidecar/Agent uses `annotations` to setup and configure. Possibly least impact
    on chart in some ways, but not compatible with our existing assumption about
    secrets being made available as environment variables (would need extra
    setup in `entrypoint` to `source /vault/secret/file` or something
- Deployments will have to specify/know the `serviceAccountName` to use that is
    granted access to the Vault in both cases
- ONLY the CSI provider supports directly mounting Secrets as environment
    variables

More links:
* [Vault Provider CSI examples][csi-examples]
* [Static Secrets: Key/Value Secrets Engine][kv-secrets-engine]
* [Vault CSI Provider vs Sidecar Injection][csi-vs-agent]

[agent-tutorial]:https://learn.hashicorp.com/tutorials/vault/kubernetes-sidecar
[csi-examples]:https://www.vaultproject.io/docs/platform/k8s/csi/examples
[csi-tutorial]:https://www.hashicorp.com/blog/retrieve-hashicorp-vault-secrets-with-kubernetes-csi
[csi-vs-agent]:https://www.hashicorp.com/blog/retrieve-hashicorp-vault-secrets-with-kubernetes-csi#vault-csi-provider-vs-sidecar-injection
[helm]:https://helm.sh/
[k3d]:https://github.com/k3d-io/k3d
[kv-secrets-engine]:https://learn.hashicorp.com/tutorials/vault/static-secrets
[vault]:https://www.vaultproject.io/
